<?php namespace App\Transformers;

class VisitorTransformer extends Transformer
{
    public function transform($item)
    {
        return [
            'id' => $item->id,
            'name' => $item->name,
            'code_access' => $item->codeArea,
            'phone' => $item->phone,
            'formatted_phone_number' => $item->phoneNumber(),
            'state' => [
                'id' => $item->state->id,
                'name' => $item->state->name
            ]
        ];
    }
}
