<?php namespace App\Transformers;

abstract class Transformer {

    public function transformCollection($items)
    {
        dd(array_map([$this, 'transform'], $items->toArray()));
        return array_map([$this, 'transform'], $items->toArray());
    }

    public abstract function transform($item);
}