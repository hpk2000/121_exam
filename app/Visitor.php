<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $table = 'visitors';

    protected $fillable = [
        'name',
        'email',
        'codeArea',
        'phone',
        'state_id'
    ];

    public function state()
    {
        return $this->belongsTo('App\State', 'state_id', 'id');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_convert_case(trim($value), MB_CASE_TITLE, 'UTF-8');
    }

    public function phoneNumber()
    {
        return "({$this->codeArea}) {$this->phone}";
    }
}
