<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    protected $statusCode = 200;

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function respondNotFound($message = 'Record not found')
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    public function respondWithError($message)
    {
        return $this->respond([
            'error' => $message,
            'status_code' => $this->getStatusCode(),
            'data' => []
        ]);
    }


    public function respondSuccessfully($data)
    {
        return $this->setStatusCode(200)->respond([
            'error' => '',
            'status_code' => $this->getStatusCode(),
            'data' => $data
        ]);
    }

    public function respond($data)
    {
        return response()->json($data, $this->getStatusCode(), ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }
}
