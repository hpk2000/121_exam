<?php namespace App\Http\Controllers\Api;

use App\Transformers\VisitorTransformer;
use App\Visitor;
use Illuminate\Http\Request;

class VisitorsController extends ApiController
{
    protected $visitorTransformer;

    public function __construct()
    {
        $this->visitorTransformer = new VisitorTransformer();
    }

    public function all()
    {
        $data = [];

        foreach (Visitor::orderBy('id', 'asc')->get() as $visitor) {
            $data[] = [
                'visitor' => $this->visitorTransformer->transform($visitor)
            ];
        }
        return $this->respondSuccessfully($data);
    }

    public function show($visitorId)
    {
        $visitor = \App\Visitor::find($visitorId);

        if (!$visitor) {
            return $this->respondNotFound('Record not found');
        }

        return $this->respondSuccessfully(['visitor' => $this->visitorTransformer->transform($visitor)]);
    }

    public function lastVisitor()
    {
        $visitor = \App\Visitor::orderBy('id', 'desc')->first();

        if (!$visitor) {
            return $this->respondNotFound('Record not found');
        }

        return $this->respondSuccessfully(['visitor' => $this->visitorTransformer->transform($visitor)]);
    }

    public function delete($visitorId)
    {
        $visitor = \App\Visitor::find($visitorId);

        if (!$visitor) {
            return $this->respondNotFound('Record not found');
        }

        $visitor->delete();

        return $this->respondSuccessfully([
            'message' => 'Record deleted'
        ]);
    }

    public function update($visitorId, Request $request)
    {
        $visitor = \App\Visitor::find($visitorId);

        if (!$visitor) {
            return $this->respondNotFound('Record not found');
        }

        $this->storeVisitor($visitor, $request);

        return $this->respondSuccessfully([
            'message' => 'Record updated'
        ]);
    }

    public function save(Request $request)
    {
        $visitor = new Visitor();
        $this->storeVisitor($visitor, $request);

        return $this->respondSuccessfully([
            'message' => 'Record created'
        ]);
    }

    private function storeVisitor($visitor, $data)
    {
        $visitor->name = $data->get('name', $visitor->name);
        $visitor->codeArea = $data->get('code_area', $visitor->codeArea);
        $visitor->phone = $data->get('phone', $visitor->phone);
        $visitor->state_id = $data->get('state_id', $visitor->state_id);
        $visitor->save();

        return true;
    }
}
