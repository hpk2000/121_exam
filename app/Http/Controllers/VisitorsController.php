<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\State;
use App\Visitor;
use DB;
use Illuminate\Http\Request;


class VisitorsController extends Controller
{
    public function index()
    {
        return view('pages.visitors.index')->with([
            'visitors' => Visitor::with('state')->orderBy('name', 'asc')->get()
        ]);
    }

    public function create()
    {
        return view('pages.visitors.form')->with([
            'visitor' => null,
            'states' => ['' => 'Select your location'] + State::orderBy('name', 'asc')->pluck('name', 'id')->toArray()
        ]);
    }

    public function save(Requests\CreateVisitorRequest $request)
    {
        DB::statement("call `121.insertVisitor`('{$request->get('name')}', '{$request->get('email')}', {$request->get('codeArea')}, {$request->get('phone')}, {$request->get('state_id')})");

        return redirect()->route('visitors')->with([
            'feedback' => [
                'type' => 'success',
                'message' => 'Record added successfully'
            ]
        ]);
    }

    public function edit($visitorId)
    {
        return view('pages.visitors.form')->with([
            'visitor' => Visitor::findOrFail($visitorId),
            'states' => ['' => 'Select your location'] + State::orderBy('name', 'asc')->pluck('name', 'id')->toArray()
        ]);
    }

    public function update(Requests\EditVisitorRequest $request)
    {
        Visitor::findOrFail($request->get('visitorId'))->update($request->all());

        return redirect()->route('visitors')->with([
            'feedback' => [
                'type' => 'success',
                'message' => 'Record updated successfully'
            ]
        ]);
    }

    public function delete(Request $request)
    {
        $visitor = Visitor::find($request->get('visitorId'));

        if (!$visitor) {
            return json_encode([
                'feedback' => [
                    'type' => 'error',
                    'message' => 'Record not found'
                ]
            ]);
        }

        DB::statement("call `121.deleteVisitor`({$visitor->id})");

        return json_encode([
            'feedback' => [
                'type' => 'success',
                'message' => 'Record deleted successfully'
            ]
        ]);
    }
}
