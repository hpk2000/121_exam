<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';

    protected $fillable = [
        'name'
    ];

    public function visitors()
    {
        return $this->hasMany('App\Visitor', 'state_id', 'id');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_convert_case(trim($value), MB_CASE_TITLE, 'UTF-8');
    }
}
