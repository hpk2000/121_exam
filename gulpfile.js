const elixir = require('laravel-elixir');

//require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
        '../plugins/datatables/jquery.dataTables.min.css',
        '../plugins/datatables/fixedHeader.bootstrap.min.css',
        '../plugins/select2/css/select2.min.css',
        '../plugins/bootstrap-select/css/bootstrap-select.min.css',
        'bootstrap.min.css',
        'core.css',
        'components.css',
        'icons.css',
        'pages.css',
        'menu.css',
        'responsive.css',
        '../plugins/switchery/switchery.min.css',
        '../plugins/bootstrap-sweetalert/sweet-alert.css'
    ], 'public/css/app.css');

    mix.scripts([
        'jquery.min.js',
        'bootstrap.min.js',
        'detect.js',
        'fastclick.js',
        'jquery.blockUI.js',
        'waves.js',
        'jquery.slimscroll.js',
        'jquery.scrollTo.min.js',
        '../plugins/switchery/switchery.min.js',
        '../plugins/bootstrap-sweetalert/sweet-alert.min.js',
        //'../plugins/bootstrap-inputmask/bootstrap-inputmask.min.js',
        '../plugins/autoNumeric/autoNumeric.js',
        '../plugins/datatables/jquery.dataTables.min.js',
        '../plugins/datatables/dataTables.bootstrap.js',
        '../plugins/select2/js/select2.min.js',
        '../plugins/bootstrap-select/js/bootstrap-select.min.js',
        'jquery.core.js',
        'jquery.app.js'
    ], 'public/js/app.js');
});
