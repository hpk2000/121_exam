@extends('layouts.master')

@section('content')

    @include('partials.breadcrumb', [
        'title' => 'Visitors manager',
        'sections' => [
            0 => [
                'name' => 'Visitors',
                'isActive' => false,
                'route' => [
                    'name' => 'visitors',
                    'parameters' => []
                ]
            ],
            1 => [
                'name' => ($visitor) ? 'Edit' : 'New',
                'isActive' => true
            ],
        ]
    ])

    @if(count($errors) > 0)
        @include('partials.form-errors', ['errors' => $errors])
    @endif

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="p-20">
                <div class="">
                    {{ Form::open(['url' => ($visitor) ? route('updateVisitor') : route('saveVisitor')]) }}

                        {{ Form::hidden('visitorId', ($visitor) ? $visitor->id : '') }}

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group m-b-20">
                                    <label for="visitorForm-nameField">Name: <span class="text-danger">*</span></label>
                                    {{ Form::text('name', ($visitor) ? $visitor->name : null, ['placeholder' => 'Full name', 'autocomplete' => 'off', 'class' => 'form-control', 'id' => 'visitorsForm-nameField']) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-b-20">
                                    <label for="visitorForm-emailField">Email: <span class="text-danger">*</span></label>
                                    {{ Form::text('email', ($visitor) ? $visitor->email : null, ['placeholder' => 'example@domain.com', 'autocomplete' => 'off', 'class' => 'form-control', 'id' => 'visitorsForm-emailField']) }}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group m-b-20">
                                    <label for="visitorForm-stateField">State: <span class="text-danger">*</span></label>
                                    {{ Form::select('state_id', $states, ($visitor) ? $visitor->state_id : null, ['autocomplete' => 'off', 'class' => 'form-control', 'id' => 'visitorsForm-stateField']) }}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group m-b-20">
                                    <label for="visitorForm-codeArea">Code area: <span class="text-danger">*</span></label>
                                    {{ Form::text('codeArea', ($visitor) ? $visitor->codeArea : null, ['autocomplete' => 'off', 'placeholder' => '999', 'class' => 'form-control autonumber', 'id' => 'visitorsForm-codeArea', 'data-a-sep' => '', 'data-a-dec' => '.', 'data-m-dec' => '0', 'data-v-min' => '000', 'data-v-max' => '999']) }}
                                    <span class="help-block"><small>Only numbers</small></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group m-b-20">
                                <label for="visitorForm-phoneField">Phone number: <span class="text-danger">*</span></label>
                                {{ Form::text('phone', ($visitor) ? $visitor->phone : null, ['autocomplete' => 'off', 'placeholder' => '9999999', 'class' => 'form-control autonumber', 'id' => 'visitorsForm-phoneField', 'data-a-sep' => '', 'data-a-dec' => '.', 'data-m-dec' => '0', 'data-v-min' => '0000000', 'data-v-max' => '9999999']) }}
                                <span class="help-block"><small>Only numbers</small></span>
                            </div>
                            </div>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-success waves-effect waves-light">Save</button>
                            <a href="{{ route('visitors') }}" class="btn btn-danger waves-effect waves-light">Cancel</a>
                        </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).on('ready', init);

        function init()
        {
            $('.autonumber').autoNumeric('init', {
                lZero: 'keep'
            });

            $('#visitorsForm-stateField').select2();
        }

    </script>

@endsection