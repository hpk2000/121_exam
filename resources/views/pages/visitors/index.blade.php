@extends('layouts.master')

@section('content')

    @include('partials.breadcrumb', [
        'title' => 'Visitors list',
        'sections' => [
            0 => [
                'name' => 'Visitors',
                'isActive' => true
            ]
        ]
    ])

    <div class="row">
        <div class="col-sm-4">
            <a href="{{ route('createVisitor') }}" class="btn btn-success btn-bordered waves-effect waves-light m-b-20"><i class="fa fa-plus"></i> New</a>
        </div><!-- end col -->
    </div>

    <table id="grid" class="table table-striped table-colored table-inverse">
        <thead>
            <th>Name</th>
            <th>Email</th>
            <th>State</th>
            <th class="text-right">Phone number</th>
            <th class="text-right">Options</th>
        </thead>
        <tbody>
            @foreach($visitors as $visitor)
                <tr id="row-{{ $visitor->id }}">
                    <td>{{ $visitor->name }}</td>
                    <td>{{ $visitor->email }}</td>
                    <td>{{ $visitor->state->name }}</td>
                    <td class="text-right">{{ $visitor->phoneNumber() }}</td>
                    <td class="text-right">
                        <a href="{{ route('editVisitor', $visitor->id) }}">
                            <span class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x text-info"></i>
                                <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                        <a href="javascript:void(0);" onclick="deleteVisitor({{ $visitor->id }});">
                            <span class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x text-danger"></i>
                                <i class="fa fa-trash fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection

@section('scripts')

    <script type="text/javascript">

        $(document).on('ready', init);

        function init()
        {
            initTable('grid', [0, 'asc'], [3, 4]);
        }

        function initTable(tableId, initialOrder, noSortableColumns)
        {
            $('#' + tableId).DataTable({
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': noSortableColumns
                }],
                "order": initialOrder
            });
        }

        function deleteVisitor(visitorId)
        {
            swal({
                title: "Delete record?",
                text: "You can not recover it",
                type: "warning",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Delete",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: true,
                animation: "slide-from-top"
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'post',
                        data: {visitorId: visitorId, _token: '{{ csrf_token() }}'},
                        url: '{{ route('deleteVisitor') }}',
                        success: function(response)
                        {
                            if (jsonData = validJSON(response)) {
                                setTimeout(function () {
                                    feedbackMessage(jsonData.feedback.message, jsonData.feedback.type);
                                }, 1000);

                                deleteRow('row-' + visitorId);
                            } else {
                                setTimeout(function () {
                                    feedbackMessage(response, 'warning');
                                }, 1000);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown)
                        {
                            setTimeout(function () {
                                feedbackMessage(errorThrown, 'warning');
                            }, 1000);
                        }
                    });
                }
            });
        }

        function deleteRow(rowId)
        {
            $('#' + rowId).remove();
        }

        function validJSON(str)
        {
            try {
                return JSON.parse(str);
            } catch (e) {
                return false;
            }
        }

    </script>

@endsection

@section('styles')

    <style type="text/css">

        .sorting_disabled.text-right {
            padding-right: 10px !important;
        }

    </style>

@endsection