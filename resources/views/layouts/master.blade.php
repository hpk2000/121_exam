<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="robots" content="all">

    <title>Edgar Fernandez Exam</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @yield('styles')

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{ asset('js/modernizr.min.js') }}"></script>

</head>

<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    @include('partials.header')

    @include('partials.nav')

    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END wrapper -->

<script>
    var resizefunc = [];
</script>

<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript">
    function feedbackMessage(message, type)
    {
        swal({
            title: '',
            text: message,
            type: type,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Close",
            closeOnConfirm: true
        });
    }

    @if(session('feedback'))
        setTimeout(function () {
            feedbackMessage('{{ session('feedback')['message'] }}', '{{ session('feedback')['type'] }}');
        }, 1000);
    @endif
</script>
@yield('scripts')

</body>
</html>