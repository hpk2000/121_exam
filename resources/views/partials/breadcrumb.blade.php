<div class="page-title-box">
    <h4 class="page-title">{{ $title }} </h4>
    <ol class="breadcrumb p-0 m-0">
        @foreach($sections as $section)
            @if(!$section['isActive'])
                <li>
                    <a href="{{ route($section['route']['name'], $section['route']['parameters']) }}">{{ $section['name'] }}</a>
                </li>
            @else
                <li class="active">
                    {{ $section['name'] }}
                </li>
            @endif
        @endforeach
    </ol>
    <div class="clearfix"></div>
</div>