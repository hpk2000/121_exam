<?php

use Illuminate\Database\Seeder;

class StoreProceduresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS `121.insertVisitor`;');
        DB::unprepared('DROP PROCEDURE IF EXISTS `121.deleteVisitor`;');

        $queryInsertVisitor = 'CREATE PROCEDURE `121.insertVisitor` (IN name varchar(255), IN email varchar(255), IN codeArea INT, IN phone INT, IN stateId INT)' . "\r\n"
            . 'BEGIN' . "\r\n"
            . 'INSERT INTO visitors (name, email, codeArea, phone, state_id) VALUES (name, email, codeArea, phone, stateId);' . "\r\n"
            . 'END';

        DB::unprepared($queryInsertVisitor);

        $queryDeleteVisitor = 'CREATE PROCEDURE `121.deleteVisitor` (IN visitorId INT)' . "\r\n"
            . 'BEGIN' . "\r\n"
            . 'DELETE FROM visitors WHERE id = visitorId;' . "\r\n"
            . 'END';

        DB::unprepared($queryDeleteVisitor);

    }
}
