# 121 Exam v1.0

> Author: Edgar Fernandez

> Date: October 25, 2016

> Production url: http://www.121.desarrollandoideas.com/

# Requirements

You can access the application at http://www.121.desarrollandoideas.com/ 

Because the project was developed using **Laravel 5.3** you will need to make sure your server meets the following requirements:
 > PHP >= 5.6.4
 
 > OpenSSL PHP Extension

 > PDO PHP Extension
 
 > Mbstring PHP Extension
 
 > Tokenizer PHP Extension
 
 > XML PHP Extension
 
# Installation

1. Clone repository
2. Run **composer install** command in order to install dependency packages
3. Create a MySQL database
4. Copy .env.example and named it .env in root folder
5. Set your database credentials in .env file
6. Run **php artisan migrate --seed** command to create and populate database tables
7. Run **php artisan key:generate** command
8. You are done!

# API

You can get the API documentation on the next document: https://docs.google.com/document/d/1wu7hLmckZAOBEvbjzFaLiQJyfMjTCa71WYhmDvWe778/edit?usp=sharing

It is possible to use basic authentication for the api requests so only users with valid credentials can use it. Just set to true your API_BASIC_AUTH variable in your .env file and use the default credentials **api@domain.com:default**

# Features being developed

> Add data validation to the create and update API calls

> Use store procedures that already exists in the UI version

# Notes

App features:
> MySQL store procedures for creating and deleting using UI version

> AJAX for deleting records in the UI version

> Sweet alerts to enhanced feedback messages

> Laravel good practices for maintenance and scale the project as well (routing, middlewares, packages, form validations, db migrations and seeders, database foreign keys, model relations, gulp for good front-end practices to ensure good performance such as minify and combine CSS and JS files)

> Several Javascript plugins for a better user experience

> And some other good stuff