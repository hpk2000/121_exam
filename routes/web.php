<?php

Route::get('/', [
    'as' => 'visitors',
    'uses' => 'VisitorsController@index'
]);

Route::get('new', [
    'as' => 'createVisitor',
    'uses' => 'VisitorsController@create'
]);

Route::get('edit/{visitorId}', [
    'as' => 'editVisitor',
    'uses' => 'VisitorsController@edit'
]);

Route::post('save', [
    'as' => 'saveVisitor',
    'uses' => 'VisitorsController@save'
]);

Route::post('update', [
    'as' => 'updateVisitor',
    'uses' => 'VisitorsController@update'
]);

Route::post('delete', [
    'as' => 'deleteVisitor',
    'uses' => 'VisitorsController@delete'
]);