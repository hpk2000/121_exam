<?php

use Illuminate\Http\Request;

Route::group(['prefix' => 'v1', 'middleware' => 'api.auth.basic'], function () {
    Route::get('visitors', 'Api\VisitorsController@all');
    Route::get('visitor', 'Api\VisitorsController@lastVisitor');
    Route::get('visitors/{visitorId}', 'Api\VisitorsController@show');
    Route::delete('visitors/{visitorId}', 'Api\VisitorsController@delete');
    Route::put('visitors/{visitorId}', 'Api\VisitorsController@update');
    Route::post('visitors', 'Api\VisitorsController@save');
});